# django_bootstrap_offcanvas
Django 1.6 project with Bootstrap v3 offcanvas example as the base template.

1. Checkout django_bootstrap_offcanvas

        git clone https://bitbucket.org/shtuffit/django_bootstrap_offcanvas.git

2. Sync the DB

        python manage.py syncdb

3. Enjoy Django with Bootstrap Offcanvas

        python manage.py runserver 
